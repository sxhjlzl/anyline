<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="www.twoant.net" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="format-detection" content="telephone=no">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="x5-fullscreen" content="true">

<title>${PAGE_TITLE }</title>
<link rel="icon" href="/favicon.ico" />
<link href="/wap/common/style/public.css?v=1.1" rel="stylesheet" type="text/css" />
<link href="/wap/common/style/list.css" rel="stylesheet" type="text/css" />
<link href="/wap/common/style/info.css" rel="stylesheet" type="text/css" />
<script src="/common/script/jquery.min.js"></script>
<script src="/common/script/anyline.jquery.js?v=1.2"></script>
<link href="/common/plugin/tips/tips_red.css?v=1.1" rel="stylesheet" type="text/css" />
<script src="/common/plugin/wap/layer/layer.js?v=1.1"></script>

</head>
<body>
	<div class="mobile">
		<jsp:include page="/WEB-INF/wap/home/inc/title.jsp"></jsp:include>
		<div class="w" style="min-height:500px;">
		<jsp:include page="${content_page }"></jsp:include>
		</div>
	</div>
</body>
</html>
