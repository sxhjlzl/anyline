package org.anyline.demo.json.home.controller;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.anyline.util.BasicUtil;
import org.anyline.util.FileUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

@Controller("json.home.FileController")
@RequestMapping("/js/hm/fl")
public class FileController extends BasicController{
	
	@RequestMapping("upload")
	@ResponseBody
	public String springUpload(HttpServletRequest request) throws IllegalStateException, IOException {
		String path = "";
        CommonsMultipartResolver multipartResolver=new CommonsMultipartResolver(request.getSession().getServletContext());
        if(multipartResolver.isMultipart(request)){
            MultipartHttpServletRequest multiRequest=(MultipartHttpServletRequest)request;
            Iterator iter=multiRequest.getFileNames();
            if(iter.hasNext()){
                MultipartFile file=multiRequest.getFile(iter.next().toString());
                if(file!=null){
                    path="/upload_img/"+BasicUtil.getRandomLowerString(10)+"."+FileUtil.getSuffixFileName(file.getOriginalFilename());
                    File tar = new File(request.getServletContext().getRealPath(path));
                    File dir = tar.getParentFile();
                    if(!dir.exists()){
                    	dir.mkdirs();
                    }
                    file.transferTo(tar);
                }
            }
           
        }
		return success(path);
	}

	@RequestMapping("dl")
	public String download(HttpServletRequest request, HttpServletResponse response) throws IllegalStateException, IOException {
		String filename = "a.zip";
		System.out.println(filename);
		response.setHeader("Content-Disposition", "attachment;filename=" + filename);
		response.setHeader("Content-Type", "application/octet-stream");
		response.setHeader("X-Accel-Redirect", "/file/" + filename);
		return null;
	}
	public static void main(String args[]){
		
	}
}