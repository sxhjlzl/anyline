package org.anyline.demo.wap.home.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.anyline.entity.DataRow;
import org.anyline.entity.DataSet;
import org.anyline.util.WebUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller("wap.home.BookController")
@RequestMapping("/wap/hm/bk")
public class BookController extends BasicController{
	protected String dir = "/WEB-INF/wap/home/page/book";
	/**
	 * JSP分页
	 * @return
	 */
	@RequestMapping("l_0")
	public ModelAndView list_0(HttpServletResponse response){
		ModelAndView mv = template("list_0.jsp","tmp1.jsp");
		DataSet sorts = null;
		sorts = service.query("MM_BOOK_SORT");						//从数据库中查询
		//sorts = service.cache("static_1800","MM_BOOK_SORT");		//从缓存中提取(无效或过期则刷新缓存)
		//WebUtil.encrypt(sorts, "ID");
		mv.addObject("sorts", sorts);
		DataSet set = service.query("MM_BOOK",parseConfig(2,"SORT_ID:sort+"));
		mv.addObject("set", WebUtil.encrypt(set, "ID"));
		try {
			response.setCharacterEncoding("UTF-8");
			response.setContentType("text/json; charset=UTF-8");
			response.getWriter().print(JSONObject.fromObject(mv.getModel()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * AJAX分布页
	 * @return
	 */
	@RequestMapping("l_1")
	public ModelAndView list_1(){
		ModelAndView mv = template("list_1.jsp");
		DataSet sorts = null;
		sorts = service.query("MM_BOOK_SORT");						//从数据库中查询
		//sorts = service.cache("static_1800","MM_BOOK_SORT");		//从缓存中提取(无效或过期则刷新缓存)
		WebUtil.encrypt(sorts, "ID");
		mv.addObject("sorts", sorts);
		return mv;
	}
	/**
	 * AJAX分页(下标)
	 * @return
	 */
	@RequestMapping("l_2")
	public ModelAndView list_2(){
		ModelAndView mv = template("list_2.jsp");
		DataSet sorts = null;
		sorts = service.query("MM_BOOK_SORT");						//从数据库中查询
		sorts = service.cache("static_1800","MM_BOOK_SORT");		//从缓存中提取(无效或过期则刷新缓存)
		WebUtil.encrypt(sorts, "ID");
		mv.addObject("sorts", sorts);
		return mv;
	}
	
	@RequestMapping("jl")
	@ResponseBody
	public String item(HttpServletRequest request, HttpServletResponse response){
		DataSet set = service.query("MM_BOOK",parseConfig(3,"SORT_ID:sort+"));
		request.setAttribute("set", WebUtil.encrypt(set, "ID"));
		return navi(request, response,set,"/WEB-INF/wap/home/template/data/book/item.jsp");
	}

	@RequestMapping("a")
	public ModelAndView add(){
		ModelAndView mv = template("info.jsp");
		DataSet sorts = service.cache("static_1800","MM_BOOK_SORT");
		WebUtil.encrypt(sorts, "ID");
		mv.addObject("sorts", sorts);
		return mv;
	}
	@RequestMapping("u")
	public ModelAndView update(){
		ModelAndView mv = template("info.jsp");
		DataRow row = service.queryRow("MM_BOOK", parseConfig("+ID:id+"));
		if(null == row){
			return error("图书存在");
		}
		mv.addObject("row", WebUtil.encrypt(row, "SORT_ID","ID"));
		DataSet sorts = service.cache("static_1800","MM_BOOK_SORT");
		mv.addObject("sorts", WebUtil.encrypt(sorts, "ID"));
		
		return mv;
	}
	@RequestMapping("v")
	public ModelAndView view(){
		ModelAndView mv = template("view.jsp");
		DataRow row = service.queryRow("MM_BOOK", parseConfig("+ID:id+"));
		if(null == row){
			return error("图书存在");
		}
		mv.addObject("row", WebUtil.encrypt(row, "ID"));
		return mv;
	}
}